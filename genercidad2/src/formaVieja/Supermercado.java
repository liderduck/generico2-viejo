package formaVieja;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class Supermercado {
	private String nombre;
	private List<Producto> listaProductos;
	private Supermercado mSupermercado;
	
	public Supermercado(String pNombre){
		this.nombre = pNombre;
		listaProductos = new ArrayList<Producto>();
	}
	
	public void addProducto(Producto prod){
		listaProductos.add(prod);
	}
	
	public List<Producto> filtrarCaducados(){
		List<Producto> caducados = new ArrayList<>();
		LocalDate fechaActual = LocalDate.now();
		for(Producto producto: listaProductos){
			if(producto.getFechaCaducidad().isBefore(fechaActual)){
				caducados.add(producto);
			}
		}
	return caducados;
	}
	
	public List<Producto> filtrarConPredicado(Caducados pPred){
		List<Producto> seleccionados = new ArrayList<>();
		for (Producto producto:listaProductos){
			if(pPred.test(producto)){
				seleccionados.add(producto);
			}
		}
		return seleccionados;
	}

	public List<Producto> filtrarConPredicado2(Predicate<Producto> predicate){
		List<Producto> seleccionados = new ArrayList<>();
		for (Producto producto:listaProductos){
			if(predicate.test(producto)){
				seleccionados.add(producto);
			}
		}
		return seleccionados;
	}
	
	public List<Producto> ordenarConCondicion(Comparator<Producto> pComp){
		listaProductos.sort(pComp);
		return listaProductos;
		
	}
	
	public void filtrarConStream(){
		listaProductos.stream().filter(a1->a1.getFechaCaducidad().isBefore(LocalDate.now())).map(Producto::getNombre).forEach(System.out::println);
	}
	
	public Stream<String> ordenarConStreams(){
		return listaProductos.stream().sorted((pP1,pP2)->new Float(pP2.getPrecio()).compareTo(new Float(pP1.getPrecio()))).map(Producto::getNombre);
		
	}
	
	


}
