package formaVieja;

import java.time.LocalDate;
import java.time.Month;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;

public class Principal {

	public static void main(String[] args){
		LocalDate unaFecha;
		Producto unProd;
		Supermercado unSup = new Supermercado("1");
		
		//def de prodcutos
		
		unaFecha = LocalDate.of(2017, Month.AUGUST, 23);
		unProd = new Producto("producto1",unaFecha,8);
		unSup.addProducto(unProd);
		
		unaFecha = LocalDate.of(2016, Month.APRIL, 23);
		unProd = new Producto("producto2",unaFecha,10);
		unSup.addProducto(unProd);
		
		//modo viejo
		//List<Producto> caducados =unSup.filtrarCaducados();
		
		
		//behaviour paramet...
	//	List<Producto> caducados = unSup.filtrarConPredicado(new Caducados());
		
		
		//clase anonima
		/*
		List<Producto> caducados= unSup.filtrarConPredicado2(new Predicate<Producto>(){
			public boolean test(Producto pProd){
				return pProd.getFechaCaducidad().isBefore(LocalDate.now());
			}
		});
		*/
		
		
		//ordenar por precio con comparator
	/*
		List<Producto> caducados =  unSup.ordenarConCondicion(new Comparator<Producto>(){
			public int compare(Producto pP1,Producto pP2){
				return (new Float(pP2.getPrecio()).compareTo(new Float(pP1.getPrecio())));
			}
		});
	*/
		
		//lambda expresion
	/*	
		List<Producto> caducados =  unSup.filtrarConPredicado2((Producto p)-> p.getFechaCaducidad().isBefore(LocalDate.now()));
		
		for(Producto prod:caducados){
			System.out.println(prod.getNombre());
		} 
		*/
		
		//filtrar con streams
		
	//	unSup.filtrarConStream();
		
		//ordenar con streams
		unSup.ordenarConStreams().forEach(System.out::println);
		
	}
}
