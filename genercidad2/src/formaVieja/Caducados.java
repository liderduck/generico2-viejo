package formaVieja;

import java.time.chrono.IsoChronology;
import java.util.function.Predicate;

public class Caducados implements Predicate<Producto> {

	@Override
	public boolean test(Producto pProd) {
		// TODO Auto-generated method stub
		return pProd.getFechaCaducidad().isBefore(IsoChronology.INSTANCE.dateNow());
	}

}
