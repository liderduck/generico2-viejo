package formaVieja;

import java.time.LocalDate;

public class Producto {
	
	private String nombre;
	private int seccion;
	private LocalDate fechaExposicion;
	private LocalDate fechaCaducidad;
	private float precio;
	
	public  Producto(String pNombrew,LocalDate pFechaCaducidad,int pPrecio){
		this.nombre=pNombrew;
		this.fechaCaducidad=pFechaCaducidad;
		this.precio=pPrecio;
	}
	
	public Producto(String pNombre,LocalDate pFechaCaducidad){
		this.nombre=pNombre;
		this.fechaCaducidad=pFechaCaducidad;
		
	}
	
	public LocalDate getFechaCaducidad(){
		return this.fechaCaducidad;
	}
	
	public String getNombre(){
		return this.nombre;
	}
	
	public float getPrecio(){
		return this.precio;
	}
	
}
